#ifndef __UTF8_H__
#define __UTF8_H__
#include <stdint.h>
#include <string.h>

#ifdef cplusplus
    extern "C" {
#endif

/* Function to read arbitrary memory
 * possibly in program space
*/
typedef uint8_t (*RDMEM)(const char*);


static char num_hiones(char c)
{
    char i = 0;
    while(c & ((uint8_t)0x80 >> i))
        i++;
    return i;
}

static uint8_t utf8chrlen(const char* s, RDMEM rdmem)
{
    char num_hi1s = num_hiones(rdmem(s));
    switch (num_hi1s) {
        case 6:
        case 5:
        case 4:
        case 3:
        case 2:
            { // validate the following octets
            uint8_t pos;
            for (pos = num_hi1s - 1; pos; --pos)
                if ((rdmem(s + pos) & 0xC0) != 0x80)
                    return 0;
            return num_hi1s;
            }
        case 0:
            if (rdmem(s) != '\0')
                return 1; /* ASCII */
    }
    return 0;
}

static uint8_t deref(const char* p) { return *(uint8_t*)p; } 

static int utf8nchridx(const char *s, RDMEM rdmem, int n, const char* uc)
{
    unsigned char chsize;
    unsigned char ucsize = utf8chrlen(uc, deref); 
    if (!s || !uc || ucsize == 0)
        return -1;
    const char* p = s;

    int index;
    for (index = 0; (chsize = utf8chrlen(p, rdmem)); ++index) {
        if (chsize == ucsize) {
            char i = 0;
            while (i < ucsize && rdmem(p + i) == *(uc + i))
                i++;
            if (i == ucsize)
                return index;
        }
        p += chsize;
        if (p - s >= n)
            break;
    }
    return -1;
}


#ifdef cplusplus
    }
#endif

#endif /* __UTF8_H__ */
