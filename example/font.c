#include "font.h"
#include "utf8.h"

#ifdef cplusplus
    extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>



#define ADDR(x) ((const char*)(uintptr_t)(x))
#define CONFIG_BYTE_ADDR ADDR(2)

static void read_metrics(FNFont* font, uint8_t bsize);
static char decode_metric_bsize(const FNFont* font);

bool fn_config(FNFont* font, uint8_t device_bitpath)
{
    /* Check data access function pointer */
    if (!font->read_data) {
        font->error_code = FN_ACCESS_FUNCTION_ERROR;
        return false;
    }

    /* Check signature */
    if (!(font->read_data(ADDR(0)) == 'F' && font->read_data(ADDR(1)) == 'n')) {
        font->error_code = FN_SIGNATURE_ERROR;
        return false;
    }
/*
 *  Config byte contains three fields encoded on respective bits 
 *  LSB is 0th bit
 *
 *  Bit   Len    Field
 *  -----------------
 *  0 - 3  4     bitpath
 *  4, 5   2     code size of metrics (bitlength of one metric fields)
 *  6, 7   2     version
 *
*/
    /* Read config byte */
    uint8_t config_byte = font->read_data(CONFIG_BYTE_ADDR);

    /* Check supported version, now only 1 */
    const uint8_t VERSION_MASK = 0xC0;
    uint8_t supported_version = 0x01;
    if (!((config_byte & VERSION_MASK) == (supported_version << 6))) {
        font->error_code = FN_UNSUPPORTED_VERSION;
        return false;
    } 

    /* Check bitpath */
    const uint8_t BITPATH_MASK = 0x0F;
    if ((config_byte & BITPATH_MASK) ^ device_bitpath) {
        font->error_code = FN_BITPATH_MISMATCH;
        return false;
    }

    /* Read metrics */
    char metric_bsize = decode_metric_bsize(font);
    font->error_code = 0;
    read_metrics(font, metric_bsize);
    if (font->error_code != 0)
        return false;

    /* Read offset of bitmaps */
    uint8_t AFTER_METRICS_OFFSET = 2 + 1 + (4*metric_bsize)/8;
    /* Hoping that compiler optimize this on little endian */
    font->glyph_data_start = font->read_data(ADDR(AFTER_METRICS_OFFSET + 3));
    font->glyph_data_start <<= 8;
    font->glyph_data_start += font->read_data(ADDR(AFTER_METRICS_OFFSET + 2));
    font->glyph_data_start <<= 16;
    font->glyph_data_start += font->read_data(ADDR(AFTER_METRICS_OFFSET + 1));
    font->glyph_data_start <<= 24;
    font->glyph_data_start += font->read_data(ADDR(AFTER_METRICS_OFFSET + 0));
    return true;
}

char decode_metric_bsize(const FNFont* font)
{
    uint8_t config_byte = font->read_data(ADDR(2));
    /* Read metric field size code and decode it */
    const uint8_t FIELD_BSIZE_CODE_MASK = 0x30;
    uint8_t metric_bsize_code = (config_byte & FIELD_BSIZE_CODE_MASK) >> 4;
    return (metric_bsize_code + 1)*4;
}

#define NIBHI(x) (((x) >> 4) & 0x0F)
#define NIBLO(x) ((x) & 0x0F)

void read_metrics(FNFont* font, uint8_t bsize)
{
    uint8_t B;
    switch (bsize)
    {
       case 4:
           {
           B = font->read_data(ADDR(3));
           font->width  = NIBHI(B);
           font->height = NIBLO(B);
           B = font->read_data(ADDR(4));
           font->xadvance = NIBHI(B);
           font->yadvance = NIBLO(B);
           }
       break;
       case 8:
            font->width = font->read_data(ADDR(3));
            font->height = font->read_data(ADDR(4));
            font->xadvance = font->read_data(ADDR(5));
            font->yadvance = font->read_data(ADDR(6));
       break;
       case 12:
            font->width = font->read_data(ADDR(3))*16 + NIBHI(font->read_data(ADDR(4)));
            font->height = NIBLO(font->read_data(ADDR(4)))*256 + font->read_data(ADDR(5));
            font->xadvance = font->read_data(ADDR(6))*16 + NIBHI(font->read_data(ADDR(7)));
            font->yadvance = NIBLO(font->read_data(ADDR(7)))*256 + font->read_data(ADDR(8));
       break;
       case 16:
            font->width = font->read_data(ADDR(3))*256 + font->read_data(ADDR(4));
            font->height = font->read_data(ADDR(5))*256 + font->read_data(ADDR(6));
            font->xadvance = font->read_data(ADDR(7))*256 + font->read_data(ADDR(8));
            font->yadvance = font->read_data(ADDR(9))*256 + font->read_data(ADDR(10));
       break;
       default:
            font->error_code = FN_UNSUPPORTED_METRICS;
       return;
    }
}  

#undef NIBHI
#undef NIBLO

int fn_find(const FNFont* font, const char* uch)
{
    const unsigned int CHARSET_TABLE = 2 + 1  // "Fn" + config byte
        + (4*decode_metric_bsize(font))/8 + 4;  // metrics + data_offset
    const unsigned int CHARSET_SIZE = font->glyph_data_start - CHARSET_TABLE;
    return utf8nchridx(ADDR(CHARSET_TABLE), font->read_data, CHARSET_SIZE, uch);
}             

inline int roundup8(int i)
{
   return ((i + 8 - 1)/8)*8;
}

uint8_t fn_byte(const FNFont* font, const char* uch, uint8_t byteno)
{
    // TODO: PACKED SUPPORT - checked in fn config
    // Packing should save us 1 extra glyph in 48 bytes
    // when 5x7 font is used.
    // In 256B rom it should be 5 extra characters.
    unsigned size = (font->read_data(CONFIG_BYTE_ADDR) &
            FN_SCAN_VERTICAL) ? 
            (roundup8(font->height)*font->width)/8:
            (roundup8(font->width)*font->height)/8;
    if (byteno > size)
        return 0;
    int index = fn_find(font, uch);
    if (index != -1) {
        return font->read_data(
               ADDR(font->glyph_data_start + size*index + byteno)
               );
    }
    return 0;
}

#ifdef cplusplus
    }
#endif

