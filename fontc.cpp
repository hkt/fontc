#include <fstream>
#include <iostream>
#include <optional>
#include <stdexcept>
#include <climits>
#include "fonttypes.h"
#include "stringoper.h"
#include "vector2.h"
#include <vector>

using std::vector;
using std::string;

bool g_verbose = false;

static constexpr int VERSION = 1;
constexpr const char COMMENT_CHAR = ';';
constexpr const char DEFS_START_DIRECTIVE[] = "====";
enum Error { INPUT_ERR = -1, PARSE_ERR = -2, CMDARG_ERR = -3 };

std::pair<string, string> parse_cmd_line(int, char* argv[]);

Font parse_input(std::istream* in, const string& fname);
long parse_header(Font*, std::istream* in, const string& fname, long lineno);
long parse_glyphs(Font*, std::istream* in, const string& fname, long lineno);

void encode_font(const Font*, std::ostream* out);
size_t encode_header(const Font*, std::ostream* out);
void encode_fontmap(const Font*, std::ostream* out, size_t header_size);
void encode_bitmaps(const Font*, std::ostream* out);

void error_fn(const string& msg, const string& fname = "", int lineno = 0);

int main(int argc, char** argv)
{
    string infname, outfname;
    std::tie(infname, outfname) = parse_cmd_line(argc, argv);

    std::ifstream fin(infname);
    if (!fin.good())
        return INPUT_ERR;
    auto font = parse_input(&fin, infname);
    fin.close();

    if (g_verbose)
        std::cout << font.info();
    std::sort(font.begin(), font.end());

    std::ofstream fout(outfname, std::ios::binary);
    encode_font(&font, &fout);
    fout.close();

    return 0;
}

std::pair<string, string> parse_cmd_line(int argc, char* argv[])
{
    if (argc < 1 + 1)
        error_fn("too few parameters");
    string infname, outfname;
    bool parameter_is_outfname = false;
    for (int i = 1; i < argc; ++i) {
        if (parameter_is_outfname) {
            if (outfname.empty()) {
                outfname.assign(argv[i]);
                parameter_is_outfname = false;
            }
            else {
                error_fn("output defined twice");
                exit(CMDARG_ERR);
            }
        }
        else if (argv[i][0] == '-')
            switch (argv[i][1]) {
                case 'o':
                    if (argv[i][2] != '\0')
                        outfname.assign(&argv[i][2]);
                    else
                        parameter_is_outfname = true;
                break;
                case 'v':
                    g_verbose = true;
                break;
            }
        else if (infname.empty())
            infname.assign(argv[i]);
        else {
            error_fn("input defined twice");
            exit(CMDARG_ERR);
        }
    }
    if (outfname.empty())
        outfname.assign("a.out");
    if (infname.empty()) {
        error_fn("no input file");
        exit(CMDARG_ERR);
    }
    if (outfname == infname) {
        error_fn("input and output are the same");
        exit(CMDARG_ERR);
    }
    return std::make_pair(infname, outfname);
}




Font parse_input(std::istream* in, const string& fname)
{
    int lineno = 0;
    Font font;
    lineno = parse_header(&font, in, fname, lineno);
    if (lineno == 0)
        exit(-1);
    lineno = parse_glyphs(&font, in, fname, lineno);
    if (lineno == 0)
        exit(-2);
    return font;
}

long parse_header(Font* font, std::istream* in, const string& fname, long lineno)
{
    for (string line; std::getline(*in, line);) {
        lineno++;
        erase_from(line, COMMENT_CHAR);
        if (is_ws_or_empty(line))
            continue;
        if (trim_copy(line) == DEFS_START_DIRECTIVE)
            break; // move on to char definitions
        std::vector<string> record = strsplit(line, ":");
        if (record.size() != 2) {
            error_fn("invalid field definition", fname, lineno);
            return 0;
        }
        try {
            if (record[0] == "BITPATH")
                font->bitpath.try_set(record[1]);
            else
                font->try_set(record[0], record[1]);
        }
        catch (std::runtime_error& ex) {
            error_fn(ex.what(), fname, lineno);
            return 0;
        }
    }
    return lineno;
}

long parse_glyphs(Font* font, std::istream* in, const string& fname, long lineno)
{
    static constexpr std::array<const char, 4>
        ON_CHARS = { {'#', 'I', '+', '0'} };

    static constexpr std::array<const char, 4>
        OFF_CHARS = { {' ', '.', '_', '1'} };

    for (string line; std::getline(*in, line);) {
        lineno++;
        erase_from(line, COMMENT_CHAR);
        if (is_ws_or_empty(line))
            continue;
        uint32_t unicode;
        try {
            unicode = std::stol(line, nullptr, 16);
        } catch (std::invalid_argument& ex) {
            error_fn("invalid unicode: " + line, fname, lineno);
        }
        Glyph glyph(unicode);
        for (string line; std::getline(*in, line);) {
            lineno++;
            erase_from(line, COMMENT_CHAR);
            if (line.size() != font->width()) {
                if (line.size() > font->width())
                    error_fn("glyph line too wide: " + line, fname, lineno);
                else
                    error_fn("glyph line too short: " + line, fname, lineno);
                return 0;
            }
            glyph.add_line(line, ON_CHARS, OFF_CHARS);
            if (glyph.size().y == font->height()) {
                font->add(std::move(glyph));
                break;
            }
        }
    }
    return lineno;
}

void encode_font(const Font* pfont, std::ostream* pout)
{
    size_t header_size = encode_header(pfont, pout);
    encode_fontmap(pfont, pout, header_size);
    encode_bitmaps(pfont, pout);
}

size_t encode_header(const Font* font, std::ostream* out)
{
    enum {
        BITS4 = 0x00,
        BITS8 = 0x01,
        BITS12 = 0x02,
        BITS16 = 0x03
    } field_bsize_code;

    auto maxval = std::max({font->width(), font->height(),
                           font->xadvance(), font->yadvance()});

    if (maxval <= 0x000F) field_bsize_code = BITS4;
        else if (maxval <= 0x00FF) field_bsize_code = BITS8;
            else if (maxval <= 0x0FFF) field_bsize_code = BITS12;
                else
                    field_bsize_code = BITS16;

    static constexpr int VERSION_BSIZE = 2;
    static constexpr int FIELD_BSIZE_CODE_BSIZE = 2;
    static_assert(VERSION <= (1u << VERSION_BSIZE) - 1);
    
    uint8_t config_byte = VERSION;
    (config_byte <<= FIELD_BSIZE_CODE_BSIZE) |= field_bsize_code;
    (config_byte <<= Bitpath::BITSIZE) |= font->bitpath.to_ulong(); 

    int field_bsize = (field_bsize_code + 1)*4;
    static constexpr int field_bsize_MAX = 16;
    using Fieldset = std::bitset<4*field_bsize_MAX>;

    Fieldset metrics(font->width());
    metrics <<= field_bsize;
    metrics |= Fieldset(font->height());
    metrics <<= field_bsize;
    metrics |= Fieldset(font->xadvance());
    metrics <<= field_bsize;
    metrics |= Fieldset(font->yadvance());

    std::vector<char> buf{ 'F', 'n', static_cast<char>(config_byte) };

    for (auto rsh = 4*field_bsize - CHAR_BIT; rsh >= 0; rsh -= CHAR_BIT) {
        Fieldset tmp = metrics;
        tmp >>= rsh;
        buf.push_back(static_cast<char>(UCHAR_MAX & tmp.to_ulong()));
    }

    out->write(buf.data(), buf.size());
    return buf.size();
}

void encode_fontmap(const Font* font, std::ostream* out, size_t header_size)
{
    uint32_t glyphs_offset = header_size + sizeof(uint32_t);
    for (auto& glyph : *font) {
        glyphs_offset += uc_size_as_utf8(glyph.unicode());
    }
    for (int i = 0 ; i < 32/CHAR_BIT; ++i)
        out->put(static_cast<char>(glyphs_offset >> (CHAR_BIT*i)));
    for (auto& glyph : *font) {
        *out << uc_as_utf8(glyph.unicode());
    }
}

void encode_bitmaps(const Font* font, std::ostream* out)
{
    for (const Glyph& g : *font) {
        int byte = 0;
        int bitmap_bits = font->bitpath.num_bits(g.width(), g.height());
        for (int bitno = 0; bitno < bitmap_bits; ++bitno) {
            iv2 point = font->bitpath.point_of_bit(bitno, g.width(), g.height());
            if (point.lies_inside(0, 0, g.width(), g.height()))
                if (g.has_bit(point.x, point.y))
                    byte |= (1 << (bitno % 8));
            if ((bitno % 8 ) == 7) {
                out->put(byte);
                byte = 0;
                }
        }
        if (bitmap_bits % 8 != 0)
            out->put(byte);
    }
}

void error_fn(const string& msg, const string& fname, int line)
{
    if (!fname.empty())
        std::clog << fname << ':';
    else
        std::clog << "fontc:";

    if (line != 0)
        std::clog << line << ':';

    std::clog << " error: " << msg << '\n';
}
