#ifndef __STRINGOPER_H__
#define __STRINGOPER_H__

#include <algorithm> 
#include <cctype>
#include <locale>
#include <vector>


// trim from start (in place)
inline void ltrim(std::string& s)
{
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
        return !std::isspace(ch);
    }));
}

// trim from end (in place)
inline void rtrim(std::string& s)
{
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

// trim from both ends (in place)
inline void trim(std::string &s)
{
    ltrim(s);
    rtrim(s);
}

// trim from start (copying)
inline std::string ltrim_copy(std::string s)
{
    ltrim(s);
    return s;
}

// trim from end (copying)
inline std::string rtrim_copy(std::string s)
{
    rtrim(s);
    return s;
}

// trim from both ends (copying)
inline std::string trim_copy(std::string s)
{
    trim(s);
    return s;
}

inline void erase_from(std::string& s, const char c)
{
    size_t p = s.find(c);
    if (p != std::string::npos)
        s.erase(p);
}


inline bool is_ws_or_empty(const std::string& s)
{
    if (s.empty())
        return true;
    size_t p = s.find_first_not_of(" \t");
    if (p == std::string::npos)
        return true;
    return false;
}

inline std::vector<std::string>
strsplit( const std::string &s, const std::string &delimiter ){
    std::vector<std::string> ret;
    size_t start = 0;
    size_t end = 0;
    size_t len = 0;
    std::string token;
    do{ end = s.find(delimiter,start); 
        len = end - start;
        token = s.substr(start, len);
        ret.emplace_back( token );
        start += len + delimiter.length();
    }while ( end != std::string::npos );
    return ret;
}

inline std::string tolower_copy(const std::string& s)
{
    std::string d;
    d.resize(s.size());
    std::transform(s.begin(), s.end(), d.begin(), ::tolower);
    return d;
}

inline int uc_size_as_utf8(uint32_t code)
{
    if (code <= 0x7F)
       return 1;
    if (code >= 0x80 && code <= 0x7FF)
       return 2;
    if (code >= 0x800 && code <= 0xFFFF)
       return 3; 
    if (code >= 0x10000 && code <= 0x1FFFF)
       return 4;
    if (code >= 0x200000 && code <= 0x3FFFFFF)
       return 5;
    else
       return 6;
}

inline std::string uc_as_utf8(uint32_t code)
{
    if (code < 0x80) 
        return std::string(1, static_cast<char>(code));
    std::string ret;
    if (code < 0x800) {
        ret.resize(2);
        ret[0] = (code>>6) | 0xC0;
        ret[1] = (code & 0x3F) | 0x80;
    }
    else
    if (code < 0x10000) {
        ret.resize(3);
        ret[0] = (code>>12) | 0xE0;
        ret[1] = ((code>>6) & 0x3F) | 0x80;
        ret[2] = (code & 0x3F) | 0x80;
    }
    else
    if (code < 0x110000) {
        ret.resize(4);
        ret[0] = (code>>18) | 0xF0;
        ret[1] = ((code>>12) & 0x3F) | 0x80;
        ret[2] = ((code>>6) & 0x3F) | 0x80;
        ret[3] = (code & 0x3F) | 0x80;
    }
    return ret;
}

#endif /* __STRINGOPER_H__ */
