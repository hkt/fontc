CXXFLAGS=-g -std=c++17 -Wall -pedantic -Weffc++
CXX=clang++

all: fontc test

fontc: fontc.cpp stringoper.h fonttypes.h vector2.h
	$(CXX) $(CXXFLAGS) $< -o $@

test: test_target
	make -C test

clean: 
	rm fontc
	make -C test clean

.PHONY: test_target clean
