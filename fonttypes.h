#ifndef __FONTTYPES_H__
#define __FONTTYPES_H__

#include <string>
#include <iomanip>
#include <sstream>
#include <bitset>
#include <stdexcept>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include "stringoper.h"
#include "vector2.h"

using std::string;
class Glyph;

inline int roundup8(int i)
{
   return ((i + 8 - 1)/8)*8;
}

class Bitpath
{
    public:
    static constexpr int BITSIZE = 4;
    enum OPTION { VDIR, HDIR, SCAN, ALIGNMENT };
    enum { VERTICAL = 1, LEFT_TO_RIGHT = 1, BOTTOM_UP = 1, ALIGNED = 1};
    void try_set(const string& s);
    unsigned long to_ulong() const { return flags.to_ulong(); }
    int num_bits(int w, int h) const;
    iv2 point_of_bit(int n, int w, int h) const;
    private:
    std::bitset<BITSIZE> flags;
};

class Font {
    private:
    std::vector<Glyph> glyphs;
    public:
    Font():
        m_width(0), m_height(0),
        m_xadvance(0), m_yadvance(0),
        param_ptrs({ {"WIDTH", &m_width },
                     {"HEIGHT", &m_height },
                     {"XADVANCE", &m_xadvance},
                     {"YADVANCE", &m_yadvance} })
    { }
    void try_set(const string& param, const string& value);
    void add(Glyph);
    int width() const { return m_width; }
    int height() const { return m_height; }
    int xadvance() const { return m_xadvance; }
    int yadvance() const { return m_yadvance; }
    auto begin() -> decltype(glyphs)::iterator { return glyphs.begin(); }
    auto end() -> decltype(glyphs)::iterator { return glyphs.end(); }
    auto begin() const -> decltype(glyphs)::const_iterator { return glyphs.begin(); }
    auto end() const -> decltype(glyphs)::const_iterator { return glyphs.end(); }
    string info() const;
    Bitpath bitpath;
    private:
    int m_width, m_height;
    int m_xadvance, m_yadvance;
    std::unordered_map<string, int*> param_ptrs;
};


class Glyph {
   public:
   Glyph(int w, int h, uint32_t unicode):
       m_size(w, h), m_unicode(unicode)
    { }
   explicit Glyph(uint32_t unicode):
       m_size(0, 0), m_unicode(unicode)
    { }
   Glyph(Glyph&& glyph) = default;
   Glyph& operator=(const Glyph& glyph) = default; 
   bool has_bit(int x, int y) const
   {
       return m_points.find(iv2(x, y)) != m_points.end();
   }
   void set_bit(int x, int y)
   {
       m_points.emplace(iv2(x, y));
   }
   template<typename ON_C, typename OFF_C>
   void add_line(const string&, const ON_C&, const OFF_C&);
   iv2 size() const { return m_size; }
   int width() const { return m_size.x; }
   int height() const { return m_size.y; }
   uint32_t unicode() const { return m_unicode; }
   const bool operator<(const Glyph& rhs)
   {
       return m_unicode < rhs.m_unicode;
   }
   private:
   std::unordered_set<iv2> m_points;
   iv2 m_size;
   uint32_t m_unicode;
};

inline void Bitpath::try_set(const string& s)
{
    std::vector<std::string> params = strsplit(s,",");
    if (params.size() != BITSIZE) 
        throw std::runtime_error("wrong number of parameters in BITPATH");
    std::bitset<BITSIZE> values_defined;
    OPTION which;
    for (auto& param : params) {
        trim(param);
        if (param == "VERTICAL" || param == "HORIZONTAL")
            which = SCAN;
        else
        if (param == "LEFT_TO_RIGHT" || param == "RIGHT_TO_LEFT")
            which = HDIR;
        else
        if (param == "BOTTOM_UP" || param == "TOP_DOWN")
            which = VDIR;
        else
        if (param == "ALIGNED" || param == "PACKED")
            which = ALIGNMENT;
        else
            throw std::runtime_error("unknown option: " + param);
        
        if (param == "VERTICAL" ||
            param == "LEFT_TO_RIGHT" ||
            param == "BOTTOM_UP" ||
            param == "ALIGNED" ) 
            flags[which] = true;

        values_defined.set(which);
    }
    if (!values_defined.all())
        throw std::runtime_error("not all parameters set in BITPATH"); 
}

inline iv2 Bitpath::point_of_bit(int n, int w, int h) const
{
    iv2 origin(0,0), point(0,0);
    signed char xsign, ysign;
    if (flags[VDIR] == BOTTOM_UP) {
      origin.y = h - 1;
      ysign = -1;
    } else {
      origin.y = 0;
      ysign = +1;
    }
    
    if (flags[HDIR] == LEFT_TO_RIGHT) {
      origin.x = 0;
      xsign = +1;
    } else { 
      origin.x = w - 1;
      xsign = -1;
    }

    if (flags[SCAN] == VERTICAL) {
        int padding = (flags[ALIGNMENT] == ALIGNED) ? (roundup8(h) - h) : 0;
        point.x = origin.x + xsign*(n/(h + padding));
        point.y = origin.y + ysign*(n - point.x*(h + padding)); 
    } else {
        int padding = (flags[ALIGNMENT] == ALIGNED) ? (roundup8(w) - w) : 0;
        point.y = origin.y + ysign*(n/(w + padding));
        point.x = origin.x + xsign*(n - point.y*(w + padding));
    }
    
    return point;
}

inline int Bitpath::num_bits(int w, int h) const
{
    if (flags[ALIGNMENT] == ALIGNED) {
        if (flags[SCAN] == VERTICAL)
            h = roundup8(h);
        else
            w = roundup8(w);
    }
    return w*h;
}
        

inline void Font::try_set(const string& param, const string& value)
{
    auto it = param_ptrs.find(param);
    if (it != param_ptrs.end()) 
        *(it->second) = stoi(value);
    else
        throw std::runtime_error("no such parameter: " + param);
}

inline string Font::info() const
{
    std::ostringstream ostr;
    for (auto& p : param_ptrs) 
        ostr << tolower_copy(p.first) << ": " << *(p.second) << '\n';
    ostr << "number of characters: " << glyphs.size() << '\n'; 
    return ostr.str();
}

inline void Font::add(Glyph glyph)
{
    glyphs.emplace_back(std::move(glyph));
}

template<typename ON_C, typename OFF_C>
inline void Glyph::add_line(const string& line, const ON_C& on, const OFF_C& off)
{
    for (int i = 0; i < line.size(); ++i) {
        if (std::find(on.begin(), on.end(), line.at(i)) != on.end())
            set_bit(i, m_size.y);
        else
            if (std::find(off.begin(), off.end(), line.at(i)) == off.end())
                throw std::invalid_argument("invalid char in line: " + line);
    }
    m_size.x = std::max(static_cast<decltype(line.size())>(m_size.x), line.size());
    m_size.y++;
}



#endif /* __FONTTYPES_H__ */
