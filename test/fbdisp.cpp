#include <cstdint>
#include <cstdio>
#include <string>
#include <cstdlib>
#include "fbdisp.h"

fb_var_screeninfo disp_info = {
    101, // Original 101,
    8,
    1
};


void fbdisp_init(Screen* scr)
{
    scr->vbuf = (uint8_t*) malloc(disp_info.xres * disp_info.yres/8);
    scr->pos = 0;
}

void fbdisp_send_data(Screen* scr, uint8_t byte)
{
    scr->vbuf[scr->pos++] = byte;
    if (scr->pos == disp_info.xres * disp_info.yres/8)
        scr->pos = 0; 
}

void fbdisp_clear(Screen* scr)
{
    for (decltype(disp_info.yres) y = 0; y < disp_info.yres; ++y)
        for (decltype(disp_info.xres) x = 0; x < disp_info.xres; ++x)
            fbdisp_set_px(scr, x, y, 0);
}

void fbdisp_set_px(Screen* scr, unsigned x, unsigned y, char value)
{
   int n = y/8;
   if (value)
        scr->vbuf[n*disp_info.xres + x] |=  (1 << (y%8));
   else
        scr->vbuf[n*disp_info.xres + x] &=  ~(1 << (y%8));
}


char fbdisp_px(Screen* scr, int x, unsigned int y) {
    if (y < disp_info.yres) {
        int n = y/8;
        if (scr->vbuf[n*disp_info.xres + x] &  (1 << (y%8)))
            return '#';
    }
    return '.';
}

std::string fbdisp_line(Screen* scr, unsigned int y) {
    std::string ret;
    if (y < disp_info.yres) {
       for (decltype(disp_info.xres) x = 0; x <= disp_info.xres; ++x)
           ret.append(1, fbdisp_px(scr, x, y));
    }
    return ret;
}

void fbdisp_send_cmd(uint8_t byte) {

}

void fbdisp_free(Screen* scr)
{
    free(scr->vbuf);
}
