#include "vector2.h"

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

TEST_CASE("[iv2] Test for lies_inside()") {
    REQUIRE(iv2(2,2).lies_inside(2, 2, 2, 2));
    REQUIRE(iv2(2,2).lies_inside(-1, -1, 5, 5));
    REQUIRE(iv2(2,2).lies_inside(1, 0, 5, 4));
    REQUIRE(iv2(2,2).lies_inside(0, 1, 5, 4));
    REQUIRE(iv2(2,2).lies_inside(0, 1, 4, 5));
}
