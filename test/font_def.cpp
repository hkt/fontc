/* Test glyph is xadvance: 6, yadvance: 8 */
/* Defined area's width: 5 x 6 */

const int XADVANCE = 6;
const int YADVANCE = 8;
const int WIDTH = 5;
const int HEIGHT = 6;

const char* LETTER_R[HEIGHT] = {
"####.",
"#...#",
"####.",
"#..#.",
"#...#",
"....."
};

const char* LETTER_F[HEIGHT] = {
"#####",
"#....",
"####.",
"#....",
"#....",
"....."
};
