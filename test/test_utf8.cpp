
#define CATCH_CONFIG_MAIN
#include <iostream>
#include <cstdint>
#include "catch.hpp"
#include "utf8.h"

TEST_CASE("num_hiones", "[utf8]") {
    const uint8_t t[] = { 0x00, 0x01, 0x02, 0x04,
                          0x80, 0x90, 0xA0, 0xAA,
                          0xD0, 0xC0, 0xE0, 0xF0,
                          0xF8, 0xFC, 0xFE, 0xFF };
    const char exp[sizeof(t)/sizeof(t[1])] = {
                          0, 0, 0, 0,
                          1, 1, 1, 1,
                          2, 2, 3, 4,
                          5, 6, 7, 8 };
    for (size_t i = 0; i < sizeof(t)/sizeof(t[0]) ; ++i) {
        REQUIRE(num_hiones(t[i]) == exp[i]);
    }
}


TEST_CASE("utf8nchrlen", "[utf8]") {
    const char* const t1[] = {
        "", "A", u8"\u0141", u8"\u0800",
        u8"\U00010000", u8"\U00200000", u8"\U04000000" };

    const char* const t2[] = {
        "", u8"\u007F", u8"\u07FF", u8"\uFFFF",
        u8"\U001FFFFF", u8"\U03FFFFFF", u8"\U7FFFFFFF" };

    for (size_t i = 0; i < sizeof(t1)/sizeof(t1[0]) ; ++i) {
        REQUIRE(utf8chrlen(t1[i], deref) == i);
    }

    for (size_t i = 0; i < sizeof(t2)/sizeof(t2[0]) ; ++i) {
        REQUIRE(utf8chrlen(t2[i], deref) == i);
    }
}

TEST_CASE("utf8nchridx", "[utf8]") {
    const char* const t = "ABC\u0141\uFFFF"; 
    REQUIRE(utf8nchridx(t, deref, 5, "A") == 0);
    REQUIRE(utf8nchridx(t, deref, 5, "B") == 1);
    REQUIRE(utf8nchridx(t, deref, 5, "C") == 2);
    REQUIRE(utf8nchridx(t, deref, 5, "\uFFFF") == -1);
}
