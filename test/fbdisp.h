#ifndef __FBDISP_H__
#define __FBDISP_H__
#include <stdint.h>
#include <string>


typedef struct {
    uint32_t xres;
    uint32_t yres;
    uint32_t bits_per_pixel;
} fb_var_screeninfo; 

typedef struct {
    uint16_t capabilities;
} fb_fix_screeninfo;

typedef struct {
    uint8_t* vbuf;
    unsigned int pos;
} Screen;

void fbdisp_init(Screen*);

void fbdisp_clear(Screen*);

void fbdisp_set_px(Screen*, unsigned x, unsigned y, char value);

void fbdisp_send_data(Screen*, uint8_t byte);

void fbdisp_send_cmd(uint8_t byte);

std::string fbdisp_line(Screen*, unsigned int);

void console_test();

void fbdisp_free(Screen*);

#endif /* __FBDISP_H__ */

