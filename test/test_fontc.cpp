#define CATCH_CONFIG_MAIN
#include <iostream>
#include <cstdlib>
#include "catch.hpp"
#include "fbdisp.h"
#include "example/font.h"

static FILE* file;
extern fb_var_screeninfo disp_info;

#include "font_def.cpp"

void create_font_file(const char* filename, std::string);
uint8_t read_function(const char* offset);

TEST_CASE("compile font straight", "[fontc]")
{
    create_font_file("test_font.txt",
            "VERTICAL,ALIGNED,LEFT_TO_RIGHT,TOP_DOWN");
    REQUIRE(system("../fontc -o test_font test_font.txt") == 0);
}

TEST_CASE("compile font flipped", "[fontc]")
{
    create_font_file("test_font_flipped.txt", 
            "VERTICAL,ALIGNED,LEFT_TO_RIGHT,BOTTOM_UP");
    REQUIRE(system("../fontc -o test_font_flipped test_font_flipped.txt") == 0);
}

TEST_CASE("load font", "[decode][display]")
{
    file = fopen("test_font",  "r");
    FNFont font;
    font.read_data = read_function;
    fn_config(&font, 0xE);

    SECTION("metrics") {
        REQUIRE(font.width == WIDTH);
        REQUIRE(font.height == HEIGHT);
        REQUIRE(font.xadvance == XADVANCE);
        REQUIRE(font.yadvance == YADVANCE);
    }

    SECTION("find characters") {
        REQUIRE(fn_find(&font, "F") == 0);
        REQUIRE(fn_find(&font, "R") == 1);
        REQUIRE(fn_find(&font, "Q") == -1);
    }

    SECTION("display on fake device") {
        Screen scr;
        fbdisp_init(&scr);
        std::cout << "\nCLEAR\n\n";
        fbdisp_clear(&scr);
        for (unsigned int l_no = 0; l_no < disp_info.yres; ++l_no) {
            std::cout << fbdisp_line(&scr, l_no) << '\n'; 
        } 
        std::cout << "\nDRAW\n\n";
        for (unsigned int i = 0; i < font.width; ++i)
            fbdisp_send_data(&scr, fn_byte(&font, "F", i));
        
        for (int i = 0; i < font.xadvance - font.width; ++i)
            fbdisp_send_data(&scr, 0x00);

        for (unsigned int i = 0; i < font.width; ++i)
            fbdisp_send_data(&scr, fn_byte(&font, "R", i));

        for (unsigned int l_no = 0; l_no < disp_info.yres; ++l_no) {
            std::cout << fbdisp_line(&scr, l_no) << '\n'; 
        } 

        for (unsigned int l_no = 0; l_no < disp_info.yres; ++l_no) {
            std::string expected;
            if (l_no < font.height) {
                expected.append(LETTER_F[l_no]);
                expected += std::string(font.xadvance - font.width, '.');
                expected.append(LETTER_R[l_no]);
                expected += std::string(font.xadvance - font.width, '.');
            }
            else
                expected = std::string(2*font.xadvance, '.');
            REQUIRE(fbdisp_line(&scr, l_no).substr(0, expected.length())
                    ==
                    expected);
        }

        fbdisp_free(&scr);
    }

    fclose(file);
}

TEST_CASE("load font flipped", "[decode][display]")
{
    file = fopen("test_font_flipped",  "r");
    FNFont font;
    font.read_data = read_function;
    fn_config(&font, 0xF);

    SECTION("metrics") {
        REQUIRE(font.width == WIDTH);
        REQUIRE(font.height == HEIGHT);
        REQUIRE(font.xadvance == XADVANCE);
        REQUIRE(font.yadvance == YADVANCE);
    }

    SECTION("find characters") {
        REQUIRE(fn_find(&font, "F") == 0);
        REQUIRE(fn_find(&font, "R") == 1);
        REQUIRE(fn_find(&font, "Q") == -1);
    }

    SECTION("display on fake device") {
        Screen scr;
        fbdisp_init(&scr);
        std::cout << "\nCLEAR\n\n";
        fbdisp_clear(&scr);
        for (unsigned int l_no = 0; l_no < disp_info.yres; ++l_no) {
            std::cout << fbdisp_line(&scr, l_no) << '\n'; 
        } 
        std::cout << "\nDRAW\n\n";
        for (unsigned int i = 0; i < font.width; ++i)
            fbdisp_send_data(&scr, fn_byte(&font, "F", i));
        
        for (int i = 0; i < font.xadvance - font.width; ++i)
            fbdisp_send_data(&scr, 0x00);

        for (unsigned int i = 0; i < font.width; ++i)
            fbdisp_send_data(&scr, fn_byte(&font, "R", i));

        for (unsigned int l_no = 0; l_no < disp_info.yres; ++l_no) {
            std::cout << fbdisp_line(&scr, l_no) << '\n'; 
        } 

        for (unsigned int l_no = 0; l_no < disp_info.yres; ++l_no) {
            std::string expected;
            if (l_no < font.height) {
                int letter_l_no_max = font.height - 1;
                expected.append(LETTER_F[letter_l_no_max - l_no]);
                expected += std::string(font.xadvance - font.width, '.');
                expected.append(LETTER_R[letter_l_no_max - l_no]);
                expected += std::string(font.xadvance - font.width, '.');
            }
            else
                expected = std::string(2*font.xadvance, '.');
            REQUIRE(fbdisp_line(&scr, l_no).substr(0, expected.length())
                    ==
                    expected);
        }

        fbdisp_free(&scr);
    }

    fclose(file);
}

void create_font_file(const char* filename, std::string bitpath)
{
    std::ofstream fout(filename);
    REQUIRE(fout.good() == true);
    fout << "WIDTH" << ':' << WIDTH << '\n';
    fout << "HEIGHT" << ':' << HEIGHT << '\n';
    fout << "XADVANCE" << ':' << XADVANCE << '\n';
    fout << "YADVANCE" << ':' << YADVANCE << '\n';
    fout << "BITPATH" << ':' << bitpath << '\n';
    fout << "====\n";

    fout << "0x46\n";
    const auto LETTER_F_HEIGHT = sizeof(LETTER_F)/sizeof(LETTER_F[0]);
    for (unsigned lineno = 0; lineno < LETTER_F_HEIGHT; ++lineno)
        fout << LETTER_F[lineno] << '\n';
    fout << '\n';

    fout << "0x52\n";
    const auto LETTER_R_HEIGHT = sizeof(LETTER_R)/sizeof(LETTER_R[0]);
    for (unsigned lineno = 0; lineno < LETTER_R_HEIGHT; ++lineno)
        fout << LETTER_R[lineno] << '\n';
    fout << '\n';
    fout.close(); 
}

uint8_t read_function(const char* offset)
{
    fseek(file, (long)offset, SEEK_SET);
    uint8_t byte;
    fread(&byte, 1, 1, file);
    return byte;
}
