#ifndef __VECTOR2_H__
#define __VECTOR2_H__

#include <utility>
#include <functional>

template <typename T>
struct Vector2 {
    Vector2(T _x, T _y): x(_x), y(_y) { }
    bool lies_inside(T, T, T, T) const;
    T x, y;
};

template <typename T>
inline bool operator==(const Vector2<T>& lhs, const Vector2<T>& rhs)
{
    return lhs.x == rhs.x && lhs.y == rhs.y;
}

template <typename T> 
inline bool Vector2<T>::lies_inside(T x1, T y1, T x2, T y2) const
{
    if (x1 > x2)
        std::swap(x1, x2);
    if (y1 > y2)
        std::swap(y1, y2);
    return (x >= x1 && x <= x2) && (y >= y1 && y <= y2);
}


namespace std {
    template<>
        struct hash<Vector2<int>> {
        std::size_t operator()(const Vector2<int>& p) const
        {
            return (p.x << 9) | p.y;
        }
    };
}

using iv2 = Vector2<int>;

#endif /* __VECTOR2_H__ */
